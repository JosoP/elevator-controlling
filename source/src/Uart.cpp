/**
 * @file    Uart.cpp
 * @date    Jan 17, 2019
 * @author  JosoP
 *
 * @brief   Source file of Uart class.
 */

#include <definesRTOS.h>
#include <Uart.h>

#include "Crc.h"
#include "fsl_uart.h"
#include "assert.h"

#include "FreeRTOS.h"
#include "task.h"
#include <semphr.h>



#define START_BYTE              0xA0                            ///< character by which all sent packets starts

#define ACK_WAITING_MS          7                               ///< waiting for ACK signal, before resent

/**
 * @struct  SHARED_DATA_t
 * @brief   Structure used for sharing data between Uart object and receiving task.
 */
typedef struct {
  volatile bool ackReceived;                // ACK flag
  QueueHandle_t queueForReceived;           // queue to which received packets will be routed
} SHARED_DATA_t;

void sendTaskBlocking(size_t length);
void receiveTaskBlocking(uint8_t* data, size_t length);
void receiveTask(void *pvParameters);
static void uart_callback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);


uint8_t           m_txBuff[MAX_PACKET_LENGTH] = { 0 };          ///< buffer for sending through UART
uint8_t           m_rxBuff[20] = { 0 };                         ///< buffer for receiving through UART
uint8_t           m_ringBuffer[40] = { 0 };                     ///< ring buffer for fsl_uart library
uart_transfer_t   m_sendXfer = { 0 };                           ///< structure containing info about sending
uart_transfer_t   recXfer = { 0 };                              ///< structure containing info about receiving
uart_handle_t     UART_handle;                                  ///< handle of uart instance (fsl_uart library)
size_t            rxSize = 0;                                   ///< received data size

SemaphoreHandle_t sendMutex;                                    ///< mutex for send function

volatile bool     rxComplete = false;                           ///< flag that receiving of data is complete
volatile bool     txComplete = false;                           ///< flag that transmitting of data is complete

SHARED_DATA_t     sharedData;                                   ///< data shared between Uart object receiving task


/********************************************************************************************************//**
 * @brief   Finds out whether packet is "ACK packet".
 *
 * @param[IN]   packet
 *
 * @return  bool - If the packet is "ACK packet" - true, otherwise - false.
 ***********************************************************************************************************/
inline bool isAck(REC_PACKET_t packet) {
    return ((packet.addrRx == 0) &&
            (packet.addrTx == 0) &&
            (packet.dataLength == 0));
}

/********************************************************************************************************//**
 * @brief   Sends data placed in m_txBuff through Uart communication in task blocking mode.
 *
 * @param[IN]   length - Number of data to send (data must be placed in m_txBuff).
 ***********************************************************************************************************/
void sendTaskBlocking(size_t length) {
  txComplete = false;

  m_sendXfer.data = m_txBuff;
  m_sendXfer.dataSize = length;
  UART_TransferSendNonBlocking(UART1, &UART_handle, &m_sendXfer);

  while (txComplete == false) {
    vTaskDelay(2);
  }
}

/********************************************************************************************************//**
 * @brief   Receives data from Uart communication in task blocking mode.
 *
 * Received data are placed to place specified by data pointer. Function will blocks task until data of
 * specified lengthis not received through the Uart communication.
 *
 * @param[OUT]  data    - Pointer to place, where received data will be stored.
 * @param[IN]   length  - Number of BYTES that will be received.
 ***********************************************************************************************************/
void receiveTaskBlocking(uint8_t* data, size_t length) {

  rxComplete = false;

  recXfer.data = data;
  recXfer.dataSize = length;

  UART_TransferReceiveNonBlocking(UART1, &UART_handle, &recXfer, &rxSize);
  while (rxComplete == false) {
    vTaskDelay(2);
  }
}

/********************************************************************************************************//**
 * @brief   Function that represents body of receive task created in the constructor of Uart object.
 *
 * This task receiving bytes until start bit comes. When start bit comes, it receives and processes packet.
 * When this packet is ACK packet, ACK flag is set. When another packet is received, it is send to receiving
 * queue, if it is registered.
 *
 * @param[IN]   pvParameters  - Parameters received when the task is created.
 ***********************************************************************************************************/
void receiveTask(void *pvParameters) {
  SHARED_DATA_t*    pShared;
  REC_PACKET_t      recMessage = { 0 };
  uint8_t           receivedByte = 0;

  pShared = (SHARED_DATA_t*) pvParameters;

  for (;;) {                                                    // infinite loop
    receiveTaskBlocking(&receivedByte, 1);                      // receive byte

    if (receivedByte == START_BYTE) {                           // if start bit has been received
      Crc countedCrc;                                           // new CRC created

      receiveTaskBlocking(&recMessage.addrRx, 1);               // RX address received
      countedCrc.calculate(recMessage.addrRx);                  // RX address added to CRC calculation

      receiveTaskBlocking(&recMessage.addrTx, 1);               // TX address received
      countedCrc.calculate(recMessage.addrTx);                  // TX address added to CRC calculation

      receiveTaskBlocking(&recMessage.dataLength, 1);           // length of the data received

      if(recMessage.dataLength){                                // if any data may be received
        receiveTaskBlocking(recMessage.data,
                            recMessage.dataLength);             // data received
        countedCrc.calculateMore(recMessage.data,
                                 recMessage.dataLength);        // data added to CRC calculation
      }
      receiveTaskBlocking(&recMessage.crc, 1);                  // CRC received

      if (recMessage.crc == countedCrc.getCurrentValue()) {     // arrived data OK?
        if(isAck(recMessage)){                                  // received packet is ACK packet
          pShared->ackReceived = true;                          // ACK flag set to unblock transmit func
        } else {                                                // received packet is not ACK packet
          if(pShared->queueForReceived != nullptr){             // if the handler registered
            xQueueSend(pShared->queueForReceived,               // send message to queue
                       &recMessage,
                       0);
          }
        }
      }
    }
  }
}

/********************************************************************************************************//**
 * @brief   Callback function called from fls_uart functions.
 *
 * @param[IN]   base      - UART peripheral base address.
 * @param[IN]   handle    - UART handle pointer.
 * @param[IN]   status    - The callback function.
 * @param[IN]   userData  - The parameter of the callback function.
 ***********************************************************************************************************/
void uart_callback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData) {
  userData = userData;

  if (kStatus_UART_TxIdle == status) {
    txComplete = true;
  }
  if (kStatus_UART_RxIdle == status) {
    rxComplete = true;
  }
}

/********************************************************************************************************//**
 * @brief   Constructor that creates and initializes Uart object, some global variables and starts rec task.
 *
 * In this constructor not only Uart object is initialized, but all components that this object uses too.
 * These are the following: global variables for UART sending, receiving, ring buffer, flags and others
 * variables that has to be global for better use.
 * In these constructor receiving task is started. This task executes receiving of packets. Other received
 * data, that are not in packet format are ignored. To reach this data (except ACK packets), receiving queue
 * has to be registered (registerRecQueue function).
 *
 * @param[IN]   baudRate  - Baud rate of UART communication.
 ***********************************************************************************************************/
Uart::Uart(uint32_t baudRate) {
  uart_config_t uart_config;

  UART_GetDefaultConfig(&uart_config);
  uart_config.baudRate_Bps = baudRate;
  UART_Init(UART1,
            &uart_config,
            CLOCK_GetFreq(kCLOCK_PllFllSelClk) / 2);         // I don't know why, but UART1 need /2
  UART_EnableTx(UART1, true);
  UART_EnableRx(UART1, true);
  UART_TransferCreateHandle(UART1, &(UART_handle), uart_callback, NULL);
  UART_TransferStartRingBuffer(UART1, &UART_handle, m_ringBuffer, sizeof(m_ringBuffer));

  sharedData.ackReceived = false;
  sharedData.queueForReceived = nullptr;

  sendMutex = xSemaphoreCreateMutex();                          // mutex creating
  assert(sendMutex);                                            // ensure to mutex exist
  xSemaphoreGive(sendMutex);                                    // first mutex give

  xTaskCreate(                                                  // creating of receiver task
      receiveTask,                          /* Function that implements the task. */
      "receiveTask",                        /* Text name for the task. */
      UART_REC_TASK_STACK_SIZE,             /* Stack size in words, not bytes. */
      (void*)&sharedData,                   /* Parameter passed into the task. */
      UART_REC_TASK_PRIORITY,               /* Priority at which the task is created. */
      NULL);                                /* Used to pass out the created task's handle. */
}

/********************************************************************************************************//**
 * @brief   Empty destructor.
 ***********************************************************************************************************/
Uart::~Uart() {
}

/********************************************************************************************************//**
 * @brief   Registers handle to queue where received packets (excepts ACK) will be sending.
 *
 * The handle to queue must be previously initialized by xQueueCreate function. Size of one element of queue
 * must be sizeof(REC_PACKET_t). When queue size (count of elements) will be too small, received packets will
 * be discarded.
 *
 * @param[IN]   queue - Handle to queue, where the packets will be sent.
 ***********************************************************************************************************/
void Uart::registerRecQueue(QueueHandle_t queue) {
  sharedData.queueForReceived = queue;
}

/********************************************************************************************************//**
 * @brief   Sends packet through Uart in format for Elevator simulation3. If ACK does not arrive in time
 *          it sends the packet again and again until the ACK arrive.
 *
 * @param[IN]   addrRX      - Address of device to which packet is sent.
 * @param[IN]   addrTX      - Address of device that sends the packet.
 * @param[IN]   data        - Pointer to the data.
 * @param[IN]   dataLength  - Length of the data.
 ***********************************************************************************************************/
void Uart::sendPacket(uint8_t addrRX, uint8_t addrTX, const uint8_t* data, uint8_t dataLength) {
  Crc       CRC;
  uint16_t  index;
  uint16_t  msCounter;

  if( xSemaphoreTake( sendMutex, portMAX_DELAY ) ){

    CRC.calculate(addrRX);                                        // calculate CRC of the message
    CRC.calculate(addrTX);
    CRC.calculateMore(data, dataLength);

    sharedData.ackReceived = false;

    do{
      index = 0;
      m_txBuff[index++] = START_BYTE;                             // start byte -> packet
      m_txBuff[index++] = addrRX;                                 // receiver address -> packet
      m_txBuff[index++] = addrTX;                                 // transmitter address -> packet
      m_txBuff[index++] = dataLength;                             // length of the data -> packet

      for (uint8_t i = 0; i < dataLength; i++) {
        m_txBuff[index++] = data[i];                              // data -> packet
      }
      m_txBuff[index++] = CRC.getCurrentValue();                  // CRC -> packet

      sendTaskBlocking(index);

      for(msCounter = 0; msCounter < ACK_WAITING_MS; msCounter++ ){
        if(sharedData.ackReceived == true){
          break;
        }
        taskDelayMs(1);
      }
    }while(sharedData.ackReceived == false);

    xSemaphoreGive( sendMutex );
  }
}


