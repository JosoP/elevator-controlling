/**
 * @file    MMA8451Q.cpp
 * @date    Nov 5, 2018
 * @author  JosoP
 *
 * @brief   Source file of MMA8451Q class.
 */

#include "MMA8451Q.h"

#include "fsl_debug_console.h"
#include "fsl_port.h"
#include "pin_mux.h"

#define REG_INT_SOURCE		  0x0C    // registers definitions
#define REG_WHO_AM_I      	0x0D
#define REG_CTRL_REG_1    	0x2A
#define REG_CTRL_REG_2    	0x2B
#define REG_CTRL_REG_3    	0x2C
#define REG_CTRL_REG_4    	0x2D
#define REG_CTRL_REG_5    	0x2E
#define REG_OUT_X_MSB     	0x01
#define REG_OUT_Y_MSB     	0x03
#define REG_OUT_Z_MSB     	0x05
#define REG_XYZ_DATA_CFG  	0x0E

#define REG_PULSE_CFG		    0x21
#define DPA			  1<<7
#define ELE			  1<<6
#define ZDPEFE		1<<5
#define ZSPEFE		1<<4
#define YDPEFE		1<<3
#define YSPEFE		1<<2
#define XDPEFE		1<<1
#define XSPEFE		1<<0

#define REG_PULSE_SRC		    0x22
#define SRC_EA		1<<7
#define SRC_AxZ		1<<6
#define	SRC_AxY		1<<5
#define SRC_AxX		1<<4
#define SRC_DPE		1<<3
#define SRC_PoIZ	1<<2
#define SRC_PoIY	1<<1
#define SRC_PoIX	1<<0

#define REG_PULSE_THSX		  0x23
#define REG_PULSE_THSY		  0x24
#define REG_PULSE_THSZ		  0x25
#define REG_PULSE_TMLT		  0x26
#define REG_PULSE_LTCY		  0x27
#define REG_PULSE_WIND		  0x28


#define UINT14_MAX        	16383                               ///< maximal number in 14-bit variable

#if defined(__cplusplus)
extern "C" {
#endif

void (*callbackFunctionACCEL) (void) = nullptr;			            ///< pointer to callback function


/********************************************************************************************************//**
 * @brief		Handler of PORTA external interrupts.
 *
 * @return		nothing
 ***********************************************************************************************************/
void PORTA_IRQHandler(void){

	PORT_ClearPinsInterruptFlags(BOARD_INITPINS_ACCEL_INT1_PORT, 1 << BOARD_INITPINS_ACCEL_INT1_PIN);

	if(callbackFunctionACCEL != nullptr)
		callbackFunctionACCEL();                                    // calling of callback function
}


#if defined(__cplusplus)
}
#endif


/********************************************************************************************************//**
 * @brief		Constructor
 *
 * @param[IN]	addr - I2C address of accelerometer. (not including I2C read/write bit)
 ***********************************************************************************************************/
MMA8451Q::MMA8451Q(int addr)
: m_i2c()
,m_addr(addr)
{
	uint8_t pom;
	this->reset();
    // activate the peripheral
	writeReg(REG_CTRL_REG_1, 0x0D);								// ODR = 400Hz, Reduced noise, Active
	writeReg(REG_XYZ_DATA_CFG, 0x00);							// +/-2g range -> 1g = 16384/4 = 4096 counts
	writeReg(REG_CTRL_REG_2, 0x02);								// High Resolution mode
	pom = readReg(REG_CTRL_REG_1);
	pom = pom;
}

/********************************************************************************************************//**
 * @brief		Destructor
 ***********************************************************************************************************/
MMA8451Q::~MMA8451Q() { }


/********************************************************************************************************//**
 * @brief		Checks whether MCU is connected with accelerometer through I2C.
 *
 * @return	If MCU - accelerometer connection is OK - true, otherwise - false.
 ***********************************************************************************************************/
bool MMA8451Q::isConnected() {
    if(readReg(REG_WHO_AM_I) == 0x1a)
    	return true;
    return false;
}


/********************************************************************************************************//**
 * @brief		Gets acceleration in X axis.
 *
 * @return		Acceleration in X axis.
 ***********************************************************************************************************/
float MMA8451Q::getAccX() {
    return (float(getAccAxis(REG_OUT_X_MSB))/4096.0);
}


/********************************************************************************************************//**
 * @brief		Gets acceleration in Y axis.
 *
 * @return		Acceleration in Y axis.
 ***********************************************************************************************************/
float MMA8451Q::getAccY() {
    return (float(getAccAxis(REG_OUT_Y_MSB))/4096.0);
}


/********************************************************************************************************//**
 * @brief		Gets acceleration in Z axis.
 *
 * @return		Acceleration in Z axis.
 ***********************************************************************************************************/
float MMA8451Q::getAccZ() {
    return (float(getAccAxis(REG_OUT_Z_MSB))/4096.0);
}


/********************************************************************************************************//**
 * @brief		Gets acceleration in all three axis.
 *
 * @param[OUT]	pResult - Pointer to array where accelerations will be stored.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::getAccAllAxis(float * pResult) {
  pResult[0] = getAccX();
  pResult[1] = getAccY();
  pResult[2] = getAccZ();
}

/********************************************************************************************************//**
 * @brief		Enables tap detection in accelerometer.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::enableTabDetection() {

	this->activate(false);

	// enable single tab on all axis
	writeReg(REG_PULSE_CFG, ZSPEFE | YSPEFE | XSPEFE);			//Enable X, Y and Z Single Pulse
	writeReg(REG_PULSE_THSX, 0x20);								//Set X Threshold to 2.016g
	writeReg(REG_PULSE_THSY, 0x20);								//Set Y Threshold to 2.016g
	writeReg(REG_PULSE_THSZ, 0x20);								//Set Z Threshold to 2.016g
	writeReg(REG_PULSE_TMLT, 0x28);								//Set Time Limit for Tap Detection to 25 ms
	writeReg(REG_PULSE_LTCY, 0x28);								//Set Latency Time to 50 ms

	this->enableInterrupt(INT_PULSE, IPOL_HIGH, INT_PIN1);

	EnableIRQ(PORTA_IRQn);

	this->activate(true);
}


/********************************************************************************************************//**
 * @brief		Enables interrupt invoking in accelerometer.
 *
 * @param[IN]	source 		- Interrupt source that will be activated.
 * @param[IN]	polarity	- Interrupt polarity.
 * @param[IN]	pin			  - Pin to which interrupt will be routed.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::enableInterrupt(
		INT_SOURCE source, INT_POLARITY polarity,INT_PIN pin) {
	uint8_t regVal;

	// interrupt polarity set
	regVal = this->readReg(REG_CTRL_REG_3);
	writeReg(REG_CTRL_REG_3, regVal | ((polarity == IPOL_HIGH) ? 1 << 1 : 0));

	// interrupt enable
	regVal = this->readReg(REG_CTRL_REG_4);
	writeReg(REG_CTRL_REG_4, regVal | source);

	// interrupt pin set
	regVal = this->readReg(REG_CTRL_REG_5);
	writeReg(REG_CTRL_REG_5, regVal | ((pin == INT_PIN1) ? source : 0));


}


/********************************************************************************************************//**
 * @brief		Registers callback function that will be called when interrupt from accelerometer occurs.
 *
 * @param[IN]	pCallback - Pointer to callback function.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::registerHandlerInt(void (*pCallback)(void)) {

	callbackFunctionACCEL = pCallback;
}


/********************************************************************************************************//**
 * @brief		Returns interrupt sources that has occurred in accelerometer.
 *
 * @return  Interrupt sources (enum INT_SOURCE). May contains more than one.
 ***********************************************************************************************************/
uint8_t MMA8451Q::getIntSource() {
	return readReg(REG_INT_SOURCE);
}


/********************************************************************************************************//**
 * @brief		Gets direction from where tap arrived. If no tap occurs, it returns error.
 *
 * @return  Direction of tap source or DIR_ERROR when no tab detection occurs.
 ***********************************************************************************************************/
MMA8451Q::PULSE_DIR MMA8451Q::getPulseDirection() {
	uint8_t 		pulseSrc;

	pulseSrc = readReg(REG_PULSE_SRC);
	if (pulseSrc & SRC_EA) {
		if (pulseSrc & SRC_AxX) {
			if (pulseSrc & SRC_PoIX) {
				return DIR_FRONT;
			} else {
				return DIR_BACK;
			}
		} else if (pulseSrc & SRC_AxY) {
			if (pulseSrc & SRC_PoIY) {
				return DIR_LEFT;
			} else {
				return DIR_RIGHT;
			}
		} else if (pulseSrc & SRC_AxZ) {
			if (pulseSrc & SRC_PoIZ) {
				return DIR_UP;
			} else {
				return DIR_DOWN;
			}
		}
	}

	return DIR_ERROR;
}


/********************************************************************************************************//**
 * @brief		Writes data byte to specified register in accelerometer.
 *
 * @param[IN]	reg 	- Address of the register in accelerometer.
 * @param[IN]	data	- Data byte to write to the register.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::writeReg(uint8_t reg, uint8_t data) {
	uint8_t buf[2];

	buf[0] = reg;
	buf[1] = data;

    m_i2c.write(m_addr, buf, 2);
}


/********************************************************************************************************//**
 * @brief		Reads data byte from specified register in accelerometer.
 *
 * @param[IN]	reg 	- Address of the register in accelerometer.
 *
 * @return		Data byte read from the register.
 ***********************************************************************************************************/
uint8_t MMA8451Q::readReg(uint8_t reg) {
	uint8_t ret;
	bool status;

	do{
	status = m_i2c.readRegs(m_addr, reg, &ret, 1);
	}while(status == false);

	return ret;
}


/********************************************************************************************************//**
 * @brief		Reads multiple data bytes from specified registers followed one another in accelerometer.
 *
 * @param[IN]	addr 	- Address of the register in accelerometer.
 * @param[OUT]	data	- Pointer where received data will be stored.
 * @param[IN]	len		- Number of bytes that will be read.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::readMoreRegs(uint8_t addr, uint8_t * data, int len) {
	m_i2c.readRegs(m_addr, addr, data, len);
}


/********************************************************************************************************//**
 * @brief		Gets raw value of acceleration in axis specified by parameter.
 *
 * @param[IN]	addr - Address of MSB register of selected axis (REG_OUT_X_MSB, REG_OUT_Y_MSB, REG_OUT_Z_MSB)
 *
 * @return		Raw value of acceleration in 2 bytes.
 ***********************************************************************************************************/
int16_t MMA8451Q::getAccAxis(uint8_t addr) {
    int16_t acc;
    uint8_t res[2];
    readMoreRegs(addr, res, 2);

    acc = (res[0] << 6) | (res[1] >> 2);
    if (acc > UINT14_MAX/2)
        acc -= UINT14_MAX;

    return acc;
}


/********************************************************************************************************//**
 * @brief		Resets the accelerometer.
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::reset() {
	uint8_t regVal;

	this->writeReg(REG_CTRL_REG_2, 0x40);						// Reset all registers to POR values

	do{            												// Wait for the RST bit to clear
		regVal = readReg(REG_CTRL_REG_2) & 0x40;
	} while (regVal);
}


/********************************************************************************************************//**
 * @brief		Switching between sleep mode and active mode.
 *
 * @param[IN]	activate - true - active mode, false - sleep mode
 *
 * @return		nothing.
 ***********************************************************************************************************/
void MMA8451Q::activate(bool activate) {
	uint8_t regVal;

	regVal = this->readReg(REG_CTRL_REG_1);
	if(activate){
		regVal |= (1<<0);
	}else{
		regVal &= ~(1<<0);
	}
	writeReg(REG_CTRL_REG_1, regVal);
}


