/**
 * @file    Elevator.cpp
 * @date    Jan 8, 2019
 * @author  JosoP
 *
 * @brief   Source file of Elevator class.
 */

#include "Elevator.h"

#include <cstring>

#include "fsl_debug_console.h"



const uint8_t Elevator::m_OWN_ADDRESS = 0x00;
const uint8_t Elevator::m_REC_QUEUE_SIZE = 10;

/********************************************************************************************************//**
 * @brief   Constructor.
 ***********************************************************************************************************/
Elevator::Elevator() :
  m_serial(57600U)
{
  m_recPackets = xQueueCreate(m_REC_QUEUE_SIZE, sizeof(REC_PACKET_t));
  m_serial.registerRecQueue(m_recPackets);
}

/********************************************************************************************************//**
 * @brief   Destructor.
 ***********************************************************************************************************/
Elevator::~Elevator() {
}

/********************************************************************************************************//**
 * @brief   Resets all elevator components (turns it of and clear). Restores watch dog.
 *
 ***********************************************************************************************************/
void Elevator::reset() {
  this->stopEngine();
  this->setAllLedsIN(OFF);
  this->setAllLedsOUT(OFF);
  this->setDisplay(NONE, "");
  this->printText("\n\r\n\r\n\r\n\r\n\r\n\r\n\r"
                  "\n\r\n\r\n\r\n\r\n\r\n\r\n\r");
  this->setEmergencyBreak(OFF);
  this->restoreWatchDog();
  this->setCabinLock(OFF);
}

/********************************************************************************************************//**
 * @brief   Prints text to console.
 *
 * @param[IN]   text  - Text that will be printed to the console.
 ***********************************************************************************************************/
void Elevator::printText(const char* text) {
  m_serial.sendPacket(
      ADDR_CONSOLE,
      m_OWN_ADDRESS,
      (const uint8_t*) text,
      strlen(text));
}

/********************************************************************************************************//**
 * @brief   Turns ON/OFF one specified elevator indoor LED.
 *
 * @param[IN]   floor - The floor on which is the specified LED placed.
 * @param[IN]   state - ON or OFF.
 ***********************************************************************************************************/
void Elevator::setLedIN(LIFT_FLOOR floor, ON_OFF state) {
  m_serial.sendPacket(
      ADDR_LED_IN_P + floor,                                    // address of 0th led + offset of led
      m_OWN_ADDRESS,
      (uint8_t*) &state,                                        // this ENUM size is 1 Byte
      1);
}

/********************************************************************************************************//**
 * @brief   Turns ON/OFF one specified elevator outdoor LED.
 *
 * @param[IN]   floor - The floor on which is the specified LED placed.
 * @param[IN]   state - ON or OFF.
 ***********************************************************************************************************/
void Elevator::setLedOUT(LIFT_FLOOR floor, ON_OFF state) {
  m_serial.sendPacket(
      ADDR_LED_OUT_P + floor,                                   // address of 0th led + offset of led
      m_OWN_ADDRESS,
      (uint8_t*) &state,                                        // this ENUM size is 1 Byte
      1);
}

/********************************************************************************************************//**
 * @brief   Turns ON/OFF all elevator indoor LEDs.
 *
 * @param[IN]   state - ON or OFF.
 ***********************************************************************************************************/
void Elevator::setAllLedsIN(ON_OFF state) {
  this->setLedIN(FLOOR_P, state);
  this->setLedIN(FLOOR_1, state);
  this->setLedIN(FLOOR_2, state);
  this->setLedIN(FLOOR_3, state);
  this->setLedIN(FLOOR_4, state);
}

/********************************************************************************************************//**
 * @brief   Turns ON/OFF all elevator outdoor LEDs.
 *
 * @param[IN]   state - ON or OFF.
 ***********************************************************************************************************/
void Elevator::setAllLedsOUT(ON_OFF state) {
  this->setLedOUT(FLOOR_P, state);
  this->setLedOUT(FLOOR_1, state);
  this->setLedOUT(FLOOR_2, state);
  this->setLedOUT(FLOOR_3, state);
  this->setLedOUT(FLOOR_4, state);
}

/********************************************************************************************************//**
 * @brief   Controls display with arrows and text.
 *
 * @param[IN]   direction - Direction of arrow that will be displayed.
 * @param[IN]   text      - Text that will be displayed.
 ***********************************************************************************************************/
void Elevator::setDisplay(DIRECTION direction, const char* text) {
  uint8_t data[STR_SIZE];
  uint8_t sizeOfText = strlen(text);
  uint8_t sizeOfData;

  m_lastDispDirection = direction;
  strcpy(m_lastDispText, text);

  if(sizeOfText < (sizeof(data) - 3)){
    data[0] = direction;                                        // sets direction BYTE
//    if(direction == NONE){
//      data[1] = data[2] = ' ';                                  // when none direction, insert 2 spaces
//      memcpy(&data[3], text, sizeOfText);
//      sizeOfData = sizeOfText + 3;
//    }else{
      memcpy(&data[1], text, sizeOfText);
      sizeOfData = sizeOfText + 1;
//    }

    m_serial.sendPacket(
        ADDR_DISPLAY,
        m_OWN_ADDRESS,
        data,
        sizeOfData);
  }
}

/********************************************************************************************************//**
 * @brief   Changes direction of arrow displayed on the display, but lets the displayed text same.
 *
 * @param[IN]   direction - Direction of arrow that will be displayed.
 ***********************************************************************************************************/
void Elevator::setDisplay(DIRECTION direction) {
  setDisplay(direction, m_lastDispText);
}

/********************************************************************************************************//**
 * @brief   Changes text displayed on the display, but lets the displayed arrow same.
 *
 * @param[IN]   text      - Text that will be displayed.
 ***********************************************************************************************************/
void Elevator::setDisplay(const char* text) {
  setDisplay(m_lastDispDirection, text);
}

/********************************************************************************************************//**
 * @brief   Converts floor to the text and sets it on the display, but lets the displayed arrow same.
 *
 * @param[IN]   floor - Floor that will be displayed as text on the display.
 ***********************************************************************************************************/
void Elevator::setDisplayFloor(LIFT_FLOOR floor) {
  switch (floor) {
  case FLOOR_P:
    setDisplay(m_lastDispDirection, "P");
    break;
  case FLOOR_1:
    setDisplay(m_lastDispDirection, "1");
    break;
  case FLOOR_2:
    setDisplay(m_lastDispDirection, "2");
    break;
  case FLOOR_3:
    setDisplay(m_lastDispDirection, "3");
    break;
  case FLOOR_4:
    setDisplay(m_lastDispDirection, "4");
    break;
  case NUMBER_OF_FLOORS:
    break;
  };
}

/********************************************************************************************************//**
 * @brief   Locks or unlocks elevator cabin of.
 *
 * @param[IN]   state - ON - locked, OFF - unlocked
 ***********************************************************************************************************/
void Elevator::setCabinLock(ON_OFF state) {
  m_serial.sendPacket(
      ADDR_CABIN,
      m_OWN_ADDRESS,
      (uint8_t*) &state,
      1);
}

/********************************************************************************************************//**
 * @brief   Activates or deactivates emergency break.
 *
 * @param[IN]   state - ON - activated, OFF - deactivated
 ***********************************************************************************************************/
void Elevator::setEmergencyBreak(ON_OFF state) {
  m_serial.sendPacket(
        ADDR_EMERGENCY_BREAK,
        m_OWN_ADDRESS,
        (uint8_t*) &state,
        1);
}

/********************************************************************************************************//**
 * @brief   Sends signal to elevator watchdog timer to regularly resets it to prevent it from elapsing.
 ***********************************************************************************************************/
void Elevator::sendWatchDogSignal() {
  uint8_t data;

  data = 0x02;
  m_serial.sendPacket(
          ADDR_WATCH_DOG,
          m_OWN_ADDRESS,
          &data,
          1);
}

/********************************************************************************************************//**
 * @brief   Reset elevator watchdog timer after "timing out"(restore watch dog timer to initial state).
 ***********************************************************************************************************/
void Elevator::restoreWatchDog() {
  uint8_t data;

  data = 0x01;
  m_serial.sendPacket(
            ADDR_WATCH_DOG,
            m_OWN_ADDRESS,
            &data,
            1);
}

/********************************************************************************************************//**
 * @brief   Sets speed of elevator engine of.
 *
 * @param[IN]   direction - Direction of Elevator cabin moving.
 * @param[IN]   speed     - Speed of Engine <0,100>.
 ***********************************************************************************************************/
void Elevator::setEngineSpeed(DIRECTION direction, int8_t speed) {
  int8_t data[5] = {0};

  if(direction == NONE) return;                                 // none direction movement not allowed

  if(0 <= speed && speed <= 100){
    data[0] = 0x02;                                             // movement command

    if(direction == DOWN) {
      data[1] = (speed != 0) ? -speed : 0;
      data[2] = 0xFF;
      data[3] = 0xFF;
      data[4] = 0xFF;
    } else {
      data[1] = speed;
    }

    m_serial.sendPacket(
            ADDR_ENGINE,
            m_OWN_ADDRESS,
            (uint8_t*)data,
            sizeof(data));
  }
}

/********************************************************************************************************//**
 * @brief   Stops elevator engine.
 ***********************************************************************************************************/
void Elevator::stopEngine() {
  uint8_t data;

  data = 0x01;
  m_serial.sendPacket(ADDR_ENGINE, m_OWN_ADDRESS, &data, 1);
}

/********************************************************************************************************//**
 * @brief   Sends request to elevator engine to send encoder value.
 ***********************************************************************************************************/
void Elevator::requestEngineEncoder() {
  uint8_t data;

  data = 0x03;
  m_serial.sendPacket(ADDR_ENGINE, m_OWN_ADDRESS, &data, 1);
}

/********************************************************************************************************//**
 * @brief   Sends request to receive status of specified sensor.
 ***********************************************************************************************************/
void Elevator::requestSensorValue(LIFT_FLOOR floor) {
  uint8_t data;

  if(floor < NUMBER_OF_FLOORS){
    data = 0x03;
    m_serial.sendPacket(ADDR_SENSOR_P + floor,
                        m_OWN_ADDRESS,
                        &data,
                        1);
  }
}

/********************************************************************************************************//**
 * @brief   Sends request to receive status of all sensors.
 ***********************************************************************************************************/
void Elevator::requestAllSensorsValue() {
  requestSensorValue(FLOOR_P);
  requestSensorValue(FLOOR_1);
  requestSensorValue(FLOOR_2);
  requestSensorValue(FLOOR_3);
  requestSensorValue(FLOOR_4);
}

/********************************************************************************************************//**
 * @brief   Gets next received packet (input). If there is not any input, it blocks the task.
 *
 * @param[OUT]  pPacket - Pointer to structure where next received packet will be stored.
 ***********************************************************************************************************/
void Elevator::getNextInput(REC_PACKET_t* pPacket) {
  xQueueReceive(m_recPackets,
                pPacket,
                portMAX_DELAY);
}
