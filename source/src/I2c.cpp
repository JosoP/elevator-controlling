/**
 * @file    I2c.cpp
 * @date    Nov 5, 2018
 * @author  JosoP
 *
 * @brief   Source file of I2c class.
 */

#include <I2c.h>
#include <stdio.h>

/********************************************************************************************************//**
 * @brief   Constructor.
 ***********************************************************************************************************/
I2c::I2c() {
  i2c_master_config_t   i2cMasterConfig;

	I2C_MasterGetDefaultConfig(&i2cMasterConfig);                 // Gets the default configuration for master.
	I2C_MasterInit(I2C0,                                          // Initialization of the I2C master.
	               &i2cMasterConfig,
	               CLOCK_GetFreq(I2C0_CLK_SRC));
}

/********************************************************************************************************//**
 * @brief   Empty destructor.
 ***********************************************************************************************************/
I2c::~I2c() {

}

/********************************************************************************************************//**
 * @brief   Writes data to the I2C device.
 *
 * @param[IN]   address - Address of the device to which the data will be send.
 * @param[IN]   pData   - Data to send.
 * @param[IN]   length  - Length of data [BYTES].
 *
 * @return  true when operation has been successful, otherwise false
 ***********************************************************************************************************/
bool I2c::write(uint8_t address, uint8_t* pData, size_t length) {
  status_t              status;
	i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Write;
	i2cMasterXfer.subaddress = 0;
	i2cMasterXfer.subaddressSize = 0;
	i2cMasterXfer.data = pData;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	status = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if(status == kStatus_Success)
		return true;
	return false;
}

/********************************************************************************************************//**
 * @brief   Reads data from the I2C device.
 *
 * @param[IN]   address - Address of the device from which will be read.
 * @param[OUT]  pData   - Pointer to palace where received data will be stored.
 * @param[IN]   length  - Size of data to read.
 *
 * @return  true when operation has been successful, otherwise false
 ***********************************************************************************************************/
bool I2c::read(uint8_t address, uint8_t* pData, size_t length) {
  status_t              status;
  i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Read;
	i2cMasterXfer.subaddress = 0;
	i2cMasterXfer.subaddressSize = 0;
	i2cMasterXfer.data = pData;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	status = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if(status == kStatus_Success)
			return true;
	return false;
}

/********************************************************************************************************//**
 * @brief   Write address of register and than reads some data from the same device.
 *
 * @param[IN]   address - Address of the device from which will be read.
 * @param[IN]   regAddr - Address of the register that will be read.
 * @param[out]  data    - Pointer to palace where received data will be stored.
 * @param[IN]   length  - Size of data to read.
 *
 * @return  true when operation has been successful, otherwise false
 ***********************************************************************************************************/
bool I2c::readRegs(uint8_t address, uint8_t regAddr, uint8_t* data, size_t length) {
	status_t              status;
	i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Read;
	i2cMasterXfer.subaddress = regAddr;
	i2cMasterXfer.subaddressSize = 1;
	i2cMasterXfer.data = data;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	status = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if (status == kStatus_Success)
		return true;
	return false;
}







