/**
 * @file    myTasks.cpp
 * @date    Jan 20, 2019
 * @author  JosoP
 *
 * @brief   Source file where main tasks and its auxiliary functions are defined.
 */

#include "myTasks.h"
#include "definesRTOS.h"
#include "event_groups.h"

void sensorSignalReceived(LIFT_FLOOR floor, SENSOR_STATE sensorState);
void arrivedToFloor(LIFT_FLOOR floor);
bool getCurrentFloor(LIFT_FLOOR* pFloor);
void findElevator(LIFT_FLOOR* pFloor);
void moveElevator(DIRECTION direction);
void slowElevatorDown();
void stopElevator();
void waitStopFloor(const EventGroupHandle_t* pEventGroup);
void elevatorIddle();
bool choooseNextFloor(LIFT_FLOOR* pFloor);
void repairBadStop(LIFT_FLOOR* pFloor);

const uint16_t WATCH_DOG_REFRESH_MS = 100;                      ///< watchdog refreshing time in milliseconds
const uint16_t LED_BLINK_PERIOD_MS = 700;                       ///< blinking of floor LEDs period

const uint8_t SPEED_FAST = 100;                                 ///< faster speed of elevator
const uint8_t SPEED_SLOW_UP = 25;                               ///< slower speed of elevator when direction is UP
const uint8_t SPEED_SLOW_DOWN = 7;                              ///< slower speed of elevator when direction is DOWN

const EventBits_t FLOOR_BIT[] = {
                                 0x000001,  ///< Event bit of Ground floor
                                 0x000002,  ///< Event bit of 1st floor
                                 0x000004,  ///< Event bit of 2nd floor
                                 0x000008,  ///< Event bit of 3rd floor
                                 0x000010   ///< Event bit of 4th floor
};                                                              ///< Event bits for every floor 0 = FLOOR_P
const EventBits_t ALL_FLOORS_BIT = 0x1F;                        ///< Event bits of all floors together

Elevator*           pElevator = nullptr;                        ///< Pointer to the instance of the elevator
QueueHandle_t       emergency = xQueueCreate(1,1);              ///< Queue of emergency. Every write to it cause emergency break activation.

EventGroupHandle_t  sensorWideFlags = xEventGroupCreate();      ///< Flags of sensors wide proximity signals
EventGroupHandle_t  sensorNarrowFlags = xEventGroupCreate();    ///< Flags of sensors narrow proximity signals
EventGroupHandle_t  reqStopFlags = xEventGroupCreate();         ///< Flags on which floors elevator stops.

QueueHandle_t engineEncoder = xQueueCreate(1, sizeof(double));  ///< Queue by which received encoder position is sent.

DIRECTION   elevatorDirection = NONE;                           ///< Current direction of the elevator.

/********************************************************************************************************//**
 * @brief   Task function used for testing.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void testTask(void *pvParameters) {


  for (;;) {
    pElevator->setAllLedsIN(OFF);
    pElevator->setAllLedsOUT(OFF);
    pElevator->setCabinLock(OFF);
    pElevator->printText("down\r\n");
    pElevator->setDisplay(DOWN, "1");

    taskDelaySec(1);

    pElevator->setAllLedsIN(ON);
    pElevator->setAllLedsOUT(ON);
    pElevator->setCabinLock(ON);
    pElevator->printText("up\r\n");
    pElevator->setDisplay(UP, "2");

    taskDelaySec(1);
  }
  vTaskDelete(NULL);
}

/********************************************************************************************************//**
 * @brief   Task function used to distribute input signals from the elevator.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void inputTask(void* pvParameters) {
  REC_PACKET_t recPacket;

  pElevator->reset();

  for (;;) {
    pElevator->getNextInput(&recPacket);

    switch(recPacket.addrTx){
    case Elevator::ADDR_BTN_OUT_P:                              // button on the P floor pressed
    case Elevator::ADDR_BTN_IN_P:
      xEventGroupSetBits(reqStopFlags, FLOOR_BIT[FLOOR_P]);     // set flag that on this floor it is needed to stop
      pElevator->setLedIN(FLOOR_P, ON);                         // turn on leds, because of immediately ON
      pElevator->setLedOUT(FLOOR_P, ON);
      break;
    case Elevator::ADDR_BTN_OUT_1:                              // button on the 1st floor pressed
    case Elevator::ADDR_BTN_IN_1:
      xEventGroupSetBits(reqStopFlags, FLOOR_BIT[FLOOR_1]);     // set flag that on this floor it is needed to stop
      pElevator->setLedIN(FLOOR_1, ON);                         // turn on leds, because of immediately ON
      pElevator->setLedOUT(FLOOR_1, ON);
      break;
    case Elevator::ADDR_BTN_OUT_2:                              // button on the 2nd floor pressed
    case Elevator::ADDR_BTN_IN_2:
      xEventGroupSetBits(reqStopFlags, FLOOR_BIT[FLOOR_2]);     // set flag that on this floor it is needed to stop
      pElevator->setLedIN(FLOOR_2, ON);                         // turn on leds, because of immediately ON
      pElevator->setLedOUT(FLOOR_2, ON);
      break;
    case Elevator::ADDR_BTN_OUT_3:                              // button on the 3rd floor pressed
    case Elevator::ADDR_BTN_IN_3:
      xEventGroupSetBits(reqStopFlags, FLOOR_BIT[FLOOR_3]);     // set flag that on this floor it is needed to stop
      pElevator->setLedIN(FLOOR_3, ON);                         // turn on leds, because of immediately ON
      pElevator->setLedOUT(FLOOR_3, ON);
      break;
    case Elevator::ADDR_BTN_OUT_4:                              // button on the 4th floor pressed
    case Elevator::ADDR_BTN_IN_4:
      xEventGroupSetBits(reqStopFlags, FLOOR_BIT[FLOOR_4]);     // set flag that on this floor it is needed to stop
      pElevator->setLedIN(FLOOR_4, ON);                         // turn on leds, because of immediately ON
      pElevator->setLedOUT(FLOOR_4, ON);
      break;
    case Elevator::ADDR_SENSOR_P:
      sensorSignalReceived(FLOOR_P, (SENSOR_STATE)recPacket.data[0]);
      break;
    case Elevator::ADDR_SENSOR_1:
      sensorSignalReceived(FLOOR_1, (SENSOR_STATE)recPacket.data[0]);
      break;
    case Elevator::ADDR_SENSOR_2:
      sensorSignalReceived(FLOOR_2, (SENSOR_STATE)recPacket.data[0]);
      break;
    case Elevator::ADDR_SENSOR_3:
      sensorSignalReceived(FLOOR_3, (SENSOR_STATE)recPacket.data[0]);
      break;
    case Elevator::ADDR_SENSOR_4:
      sensorSignalReceived(FLOOR_4, (SENSOR_STATE)recPacket.data[0]);
      break;
    case Elevator::ADDR_ENGINE:
      xQueueSend(engineEncoder,                                 // send encoder value to queue like double
                 (void*)&recPacket.data[0],
                 0);
      break;
    default:
      pElevator->printText("Unknown input signal\r\n");
    }
  }
}

/********************************************************************************************************//**
 * @brief   Task function used to control all movements of the elevator cabin.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void movementTask(void* pvParameters) {
  LIFT_FLOOR  firstReqFloor;
  LIFT_FLOOR  currentFloor;

  xEventGroupClearBits(sensorWideFlags, ALL_FLOORS_BIT);
  xEventGroupClearBits(sensorNarrowFlags, ALL_FLOORS_BIT);
  xEventGroupClearBits(reqStopFlags, ALL_FLOORS_BIT);

  pElevator->requestAllSensorsValue();
  taskDelayMs(50);
  if (!getCurrentFloor(&currentFloor)) {                        // elevator out of floors
    findElevator(&currentFloor);                                // find elevator and get current floor
  }

  pElevator->setDisplayFloor(currentFloor);

  for (;;) {
    xEventGroupWaitBits(reqStopFlags, ALL_FLOORS_BIT,           // wait for some pressed button
                        pdFALSE, pdFALSE, portMAX_DELAY);

    while (choooseNextFloor(&firstReqFloor)) {                  // do while is some request to stop
      if (currentFloor == firstReqFloor) {                      // when button where elevator stands pressed
        arrivedToFloor(firstReqFloor);
      } else {                                                  // another floor selected
        if (currentFloor > firstReqFloor) {                     // when button DOWN from the elevator pressed
          moveElevator(DOWN);
        } else
          if (currentFloor < firstReqFloor) {                   // when button UP from the elevator pressed
            moveElevator(UP);
          }

        waitStopFloor(&sensorWideFlags);                        // wait until wide proximity of stop floor
        slowElevatorDown();                                     // slow down
        waitStopFloor(&sensorNarrowFlags);                      // wait until narrow proximity of stop floor
        stopElevator();                                         // stop
        taskDelaySec(1.5);                                      // wait until the elevator really stops

        repairBadStop(&currentFloor);
        arrivedToFloor(currentFloor);
        taskDelaySec(4);
      }
    }

    elevatorIddle();
  }
}

/********************************************************************************************************//**
 * @brief   Task function used to reseting of elevator watchdog timer.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void watchDogTask(void* pvParameters) {
  for (;;) {
    pElevator->sendWatchDogSignal();                            // correctly reset watchdog
    taskDelayMs(100);
  }
}

/********************************************************************************************************//**
 * @brief   Task function used to blinking with LEDs of floors where elevator stops outside and inside the
 *          cabin.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void blinkerTask(void *pvParameters){

  for (;;) {
    for(uint8_t i = 0; i < NUMBER_OF_FLOORS; i++){
      if(xEventGroupGetBits(reqStopFlags) & FLOOR_BIT[i]){      // if there is request to stop on this floor
        pElevator->setLedIN((LIFT_FLOOR)i, ON);                 // turn ON floor LEDs
        pElevator->setLedOUT((LIFT_FLOOR)i, ON);
      }
    }
    taskDelayMs(LED_BLINK_PERIOD_MS / 4);

    pElevator->setAllLedsIN(OFF);                               // turn OFF all LEDs
    pElevator->setAllLedsOUT(OFF);
    taskDelayMs(LED_BLINK_PERIOD_MS / 4 * 3);
  }
}

/********************************************************************************************************//**
 * @brief   Task function used to change floor displayed on the display - speaks about where the elvator is
 *          right now.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void floorDiplayTask(void* pvParameters) {
  EventBits_t retBits;
  EventBits_t ignoredBits;

  taskDelayMs(10000);

  for(;;){
    ignoredBits = xEventGroupGetBits(sensorWideFlags);
    retBits = xEventGroupWaitBits(sensorWideFlags,
                                  ALL_FLOORS_BIT ^ ignoredBits,
                                  pdFALSE,
                                  pdFALSE,
                                  portMAX_DELAY);

    for(uint8_t i = 0; i < NUMBER_OF_FLOORS; i++){
      if(retBits & FLOOR_BIT[i]){                               // near this floor is the elevator
        pElevator->setDisplayFloor((LIFT_FLOOR)i);
        break;                                                  // break from for loop
      }
    }
  }
}

/********************************************************************************************************//**
 * @brief   Task function used to activate the emergency break when it is needed.
 *
 * @param[IN]   pvParameters  - Pointer to parameters sent to this task.
 ***********************************************************************************************************/
void emergencyTask(void* pvParameters) {
  uint8_t received;

  xQueueReset(emergency);
  xQueueReceive(emergency, &received, portMAX_DELAY);
  pElevator->setEmergencyBreak(ON);
  pElevator->stopEngine();
  pElevator->setDisplay(NONE, "STOP");
  xEventGroupClearBits(reqStopFlags, ALL_FLOORS_BIT);
  vTaskEndScheduler();
  for(;;);
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that processes received signal from elevators sensors.
 * @param[IN]   floor       - Floor from which the signal has arrived.
 * @param[IN]   sensorState - State of sensor from which the signal is.
 ***********************************************************************************************************/
void sensorSignalReceived(LIFT_FLOOR floor, SENSOR_STATE sensorState) {
  if (sensorNarrowFlags && sensorWideFlags) {
    switch (sensorState) {
    case SENSOR_LOW:                                            // sensor signal - elevator too far
      xEventGroupClearBits(sensorNarrowFlags, FLOOR_BIT[floor]);
      xEventGroupClearBits(sensorWideFlags, FLOOR_BIT[floor]);
      break;
    case WIDE_PROXIMITY:                                        // sensor signal - elevator nearly on the floor
    {
      EventBits_t wideBits;
      xEventGroupClearBits(sensorNarrowFlags, FLOOR_BIT[floor]);

      wideBits= xEventGroupGetBits(sensorWideFlags);
      if ((wideBits & FLOOR_BIT[floor]) == 0) {                 // set wide bit only when it is not set
        xEventGroupSetBits(sensorWideFlags, FLOOR_BIT[floor]);
      } else {
        wideBits = 3;
      }
    }

      break;
    case NARROW_PROXIMITY:                                      // sensor signal - elevator currently on the floor
      xEventGroupSetBits(sensorNarrowFlags, FLOOR_BIT[floor]);
      break;
    }
  }
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that does everything needed after stopping on some floor.
 * @param[IN]   floor - The floor on which the elevator has stopped.
 ***********************************************************************************************************/
void arrivedToFloor(LIFT_FLOOR floor){
  xEventGroupClearBits(reqStopFlags, FLOOR_BIT[floor]);         // clear request bit to stop here
  pElevator->setLedIN(floor, OFF);                              // turn OFF floor LEDs immediately
  pElevator->setLedOUT(floor, OFF);
  pElevator->setCabinLock(OFF);                                 // unlock the cabin door
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that finds out on which floor the elevator is right now. It returns value only
 *          when the elevator is right on the floor. (narrow proximity)
 * @param[OUT]  pFloor  - Pointer to place where current floor will be stored.
 * @return  true, when the elevator stands on some floor, otherwise false.
 ***********************************************************************************************************/
bool getCurrentFloor(LIFT_FLOOR* pFloor){
  EventBits_t retBits;

  retBits = xEventGroupGetBits(sensorNarrowFlags);

  for(uint8_t i = 0; i < NUMBER_OF_FLOORS; i++){
    if(retBits & FLOOR_BIT[i]){
      *pFloor = (LIFT_FLOOR)i;
      return true;
    }
  }
  return false;
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that finds out elevator position and moves with it to some floor.
 * @param[OUT]  pFloor  - Pointer to place where current floor on which the elevator stands will be stored.
 ***********************************************************************************************************/
void findElevator(LIFT_FLOOR* pFloor){
  double encoderVal;

  pElevator->setDisplay("FIX");
  do{

    xQueueReset(engineEncoder);                                 // reset queue
    pElevator->requestEngineEncoder();                          // request for elevator position
    xQueueReceive(engineEncoder, &encoderVal, portMAX_DELAY);   // read elevator position from queue

    xEventGroupSetBits(reqStopFlags, ALL_FLOORS_BIT);
    if(encoderVal < - 500){
      moveElevator(UP);
    } else {
      moveElevator(DOWN);
    }

    xEventGroupWaitBits(sensorWideFlags, ALL_FLOORS_BIT,
                        pdFALSE, pdFALSE, portMAX_DELAY);
    slowElevatorDown();

    xEventGroupWaitBits(sensorNarrowFlags, ALL_FLOORS_BIT,
                          pdFALSE, pdFALSE, portMAX_DELAY);
    stopElevator();
  } while(!getCurrentFloor(pFloor));

  taskDelaySec(1);
  pElevator->setDisplayFloor(*pFloor);
  arrivedToFloor(*pFloor);
  elevatorIddle();

  xEventGroupClearBits(reqStopFlags, ALL_FLOORS_BIT);
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that does everything needed for start and starts moving with elevator.
 * @param[IN]   direction - Direction to which the elevator will starts moving.
 ***********************************************************************************************************/
void moveElevator(DIRECTION direction){
  elevatorDirection = direction;
  pElevator->setCabinLock(ON);
  pElevator->setDisplay(direction);
  pElevator->setEngineSpeed(direction, SPEED_FAST);
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that slows the elevator to SPEED_SLOW.
 ***********************************************************************************************************/
void slowElevatorDown(){
  if(elevatorDirection == UP){
    pElevator->setEngineSpeed(elevatorDirection, SPEED_SLOW_UP);
  } else if (elevatorDirection == DOWN){
    pElevator->setEngineSpeed(elevatorDirection, SPEED_SLOW_DOWN);
  }
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that stops the elevator.
 ***********************************************************************************************************/
void stopElevator(){
  pElevator->stopEngine();
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that waits until some event bit of floor where the elevator needs to stop is
 *          set.
 * @param[IN]   pEventGroup - Pointer to event group on which bits will this function wait.
 ***********************************************************************************************************/
void waitStopFloor(const EventGroupHandle_t* pEventGroup){
  EventBits_t retBitsSensor;
  EventBits_t retBitsStopReq;
  EventBits_t ingnoredBits;

  for (;;) {
    ingnoredBits = xEventGroupGetBits(*pEventGroup);            // read currently set bits
    retBitsSensor = xEventGroupWaitBits(
        *pEventGroup,
        ALL_FLOORS_BIT ^ ingnoredBits,                          // ignore bits that has been set before
        pdFALSE,
        pdFALSE,
        portMAX_DELAY);
    retBitsStopReq = xEventGroupGetBits(reqStopFlags);

    for (uint8_t i = 0; i < NUMBER_OF_FLOORS; i++) {
      if (retBitsSensor & FLOOR_BIT[i]) {                       // i == floor on which the elevator is
        if (retBitsStopReq & FLOOR_BIT[i]) {                    // if here could elevator stop
          return;
        }
      }
    }
  }
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that does everything needed when elevator stops and there is not next request
 *          to stop on any floor.
 ***********************************************************************************************************/
void elevatorIddle(){
  elevatorDirection = NONE;
  pElevator->setDisplay(NONE);
}

/********************************************************************************************************//**
 * @brief   Auxiliary function that choose next floor to go according to current direction and the elevator
 *          current position.
 * @param[OUT]  pFloor  - Pointer to place where next chosen floor will be stored.
 * @return  true, when some floor has been chosen, otherwise false.
 ***********************************************************************************************************/
bool choooseNextFloor(LIFT_FLOOR* pFloor){
  EventBits_t retBits;

  retBits = xEventGroupGetBits(reqStopFlags);

  if (elevatorDirection == UP) {
    for (int8_t i = NUMBER_OF_FLOORS - 1; i >= 0; i--) {        // ensure which floor has been selected
      if (retBits & FLOOR_BIT[i]) {
        *pFloor = (LIFT_FLOOR) i;
        return true;
      }
    }
  } else {
    for (uint8_t i = 0; i < NUMBER_OF_FLOORS; i++) {            // ensure which floor has been selected
      if (retBits & FLOOR_BIT[i]) {
        *pFloor = (LIFT_FLOOR) i;
        return true;
      }
    }
  }
  return false;
}

/********************************************************************************************************//**
 * @brief   Repairs bad stop on the floor (when elevator miss the floor) if it is needed.
 *
 * @param[OUT]  pFloor  - Pointer to place where final floor will be stored.
 ***********************************************************************************************************/
void repairBadStop(LIFT_FLOOR* pFloor) {
  DIRECTION  repairDirection = elevatorDirection;

  while (!getCurrentFloor(pFloor)) {                            // while elevator stopped out of the floor
    repairDirection = (repairDirection == UP) ? DOWN : UP;      // reverse direction
    pElevator->setEngineSpeed(repairDirection, 10);
    waitStopFloor(&sensorNarrowFlags);                          // wait until narrow proximity of stop floor
    stopElevator();                                             // stop
  }
}


