/**
 * @file    "Elevator Controlling.cpp"
 * @date    8 Jan, 2019
 * @author  JosoP
 *
 * @brief   Application entry point. (main function is located here)
 */


#include <definesRTOS.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "MKL25Z4.h"
#include "clock_config.h"
#include "fsl_debug_console.h"

#include "definesRTOS.h"
#include "myTasks.h"
#include "MMA8451Q.h"

#define ACCEL_I2C_ADDRESS 0x1D                                  ///< I2C address of accelerometer sensor

void accelIntHandler(void);

MMA8451Q* pAccel = nullptr;                                     ///< Pointer to accelerometer object

/********************************************************************************************************//**
 * @brief   Application entry point.
 *
 * In this function only main objects are initialized and main tasks are created. After that all this APP
 * starts task scheduler of freeRTOS operating system.
 ***********************************************************************************************************/
int main(void) {

  BaseType_t creatingStatus;

  /* Init board hardware. */
  BOARD_InitBootPins();
  BOARD_InitBootClocks();
  BOARD_InitBootPeripherals();
  /* Init FSL debug console. */
  BOARD_InitDebugConsole();

  pElevator = new Elevator();
  pAccel = new MMA8451Q(ACCEL_I2C_ADDRESS);

  pAccel->registerHandlerInt(accelIntHandler);
  pAccel->enableTabDetection();

  if (pAccel->isConnected()) {
    PRINTF("Accel connected OK.\n\r");
  } else {
    PRINTF("ERROR - Accel not connected.\n\r");
  }

//  creatingStatus = xTaskCreate(testTask,"testTask", UART_REC_TASK_STACK_SIZE, NULL, UART_REC_TASK_PRIORITY, NULL);
//  if(creatingStatus != pdPASS) {
//    PRINTF("testTask creation failed!.\r\n");
//    while (1);
//  }

  creatingStatus = xTaskCreate(inputTask, "input", INPUT_TASK_STACK_SIZE, NULL, INPUT_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("inputTask creation failed!.\r\n");
    while (1);
  }

  creatingStatus = xTaskCreate(watchDogTask, "watchDog", WATCH_DOG_TASK_STACK_SIZE, NULL, WATCH_DOG_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("watchDogTask creation failed!.\r\n");
    while (1);
  }

  creatingStatus = xTaskCreate(blinkerTask, "blinker", BLINKER_TASK_STACK_SIZE, NULL, BLINKER_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("blinkerTask creation failed!.\r\n");
    while (1);
  }

  creatingStatus = xTaskCreate(movementTask, "movement", MOVEMENT_TASK_STACK_SIZE, NULL, MOVEMENT_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("movementTask creation failed!.\r\n");
    while (1);
  }

  creatingStatus = xTaskCreate(floorDiplayTask, "floorDiplay", FLOOR_DSP_TASK_STACK_SIZE, NULL, FLOOR_DSP_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("floorDiplayTask creation failed!.\r\n");
    while (1);
  }

    creatingStatus = xTaskCreate(emergencyTask, "emergency", FLOOR_DSP_TASK_STACK_SIZE, NULL, FLOOR_DSP_TASK_PRIORITY, NULL);
  if (creatingStatus != pdPASS) {
    PRINTF("emergencyTask creation failed!.\r\n");
    while (1);
  }

  vTaskStartScheduler();

  /* Force the counter to be placed into memory. */
  volatile static int i = 0;
  /* Enter an infinite loop, just incrementing a counter. */
  while (1){
    i++;
  }
  return 0;
}


/********************************************************************************************************//**
 * @brief   Callback function that handles interrupts from accelerometer.
 ***********************************************************************************************************/
void accelIntHandler(void) {
  uint8_t     src;

  src = pAccel->getIntSource();

  if (src & MMA8451Q::INT_PULSE) {
    pAccel->getPulseDirection();
    xQueueSendFromISR(emergency, &src, NULL);
  }
}
