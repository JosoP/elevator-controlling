/**
 * @file    definesRTOS.h
 * @date    Jan 17, 2019
 * @author  JosoP
 *
 * @brief   Definitions and useful macros for freertos in this project.
 */

#ifndef INC_DEFINESRTOS_H_
#define INC_DEFINESRTOS_H_

#include "FreeRTOS.h"
#include "task.h"

/// Task delay defined in milliseconds
#define taskDelayMs(miliseconds)  vTaskDelay(miliseconds / portTICK_PERIOD_MS)
/// Task delay defined in seconds
#define taskDelaySec(seconds)     vTaskDelay(((unsigned)(seconds * 1000)) / portTICK_PERIOD_MS)
/// conversion of ms to ticks
#define ms2tick(ms)               (ms /  portTICK_PERIOD_MS)

// PRIORITES OF TASKS
/// Priority of test task
#define TEST_TASK_PRIORITY        3
/// Priority of task used for receiving UART packets.
#define UART_REC_TASK_PRIORITY    3
/// Priority of task processing elevators inputs.
#define INPUT_TASK_PRIORITY       3
/// Priority of task used to controlling of all movements of the elevator cabin.
#define MOVEMENT_TASK_PRIORITY    2
/// Priority of task used to reseting elevator watchdog timer.
#define WATCH_DOG_TASK_PRIORITY   1
/// Priority of task used to blinking with LEDs.
#define BLINKER_TASK_PRIORITY     2
/// Priority of task used to changing displayed floors.
#define FLOOR_DSP_TASK_PRIORITY   2
/// Priority of task used to activate an emergency break if needed.
#define EMERGENCY_TASK_PRIORITY   4

// STACK SIZES OF TASKS
/// Size of stack for test task.[WORDS]
#define TEST_TASK_STACK_SIZE      100
/// Size of stack for task used for receiving UART packets.[WORDS]
#define UART_REC_TASK_STACK_SIZE  100
/// Priority of task processing elevators inputs.[WORDS]
#define INPUT_TASK_STACK_SIZE     100
/// Priority of task used to controlling of all movements of the elevator cabin.[WORDS]
#define MOVEMENT_TASK_STACK_SIZE  100
/// Size of stack for task used to reseting elevator watchdog timer.[WORDS]
#define WATCH_DOG_TASK_STACK_SIZE configMINIMAL_STACK_SIZE
/// Size of stack for task used to blinking with LEDs.[WORDS]
#define BLINKER_TASK_STACK_SIZE   configMINIMAL_STACK_SIZE
/// Size of stack for task used to changing displayed floors.
#define FLOOR_DSP_TASK_STACK_SIZE 100
/// Size of stack for task used to activate an emergency break if needed.[WORDS]
#define EMERGENCY_STACK_SIZE      configMINIMAL_STACK_SIZE

#endif /* INC_DEFINESRTOS_H_ */
