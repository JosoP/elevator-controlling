/**
 * @file    Elevator.h
 * @date    Jan 8, 2019
 * @author  JosoP
 *
 * @brief   Header file of Elevator class.
 */

#ifndef INC_LIFTCOMUNICATOR_H_
#define INC_LIFTCOMUNICATOR_H_

#include "Uart.h"

#include "FreeRTOS.h"
#include "queue.h"

#define STR_SIZE    30                      ///< Size of string buffers

/**
 * @enum SENSOR_STATE
 * @brief Possible states of elevator floor sensor.
 */
enum SENSOR_STATE : uint8_t {
  SENSOR_LOW        = 0x00,                 ///< elevator is too far from sensor
  WIDE_PROXIMITY    = 0x01,                 ///< elevator is in wide proximity
  NARROW_PROXIMITY  = 0x02                  ///< elevator is in narrow proximity
};

/**
 * @enum DIRECTION
 * @brief Direction of something (arrows, engine).
 *        Its values are correct for send to Elevator simulator (arrows).
 */
enum DIRECTION : uint8_t {
  UP    = 0x01,
  DOWN  = 0x02,
  NONE  = 0x03
};

/**
 * @enum LIFT_FLOOR
 * @brief Possible floors with its count in the end.
 */
enum LIFT_FLOOR : uint8_t {
  FLOOR_P,              ///< Ground floor
  FLOOR_1,              ///< 1st floor
  FLOOR_2,              ///< 2nd floor
  FLOOR_3,              ///< 3rd floor
  FLOOR_4 ,             ///< 4th floor
  NUMBER_OF_FLOORS      ///< number of floors that are in this APP
};

/**
 * @enum ON_OFF
 * @brief On or off values of components. Its values are correct for send to Elevator simulator.
 */
enum ON_OFF : uint8_t {
  OFF = 0,
  ON = 1
};

/**
 *  @class  Elevator
 *
 *  @brief  Class that represents Elevator and it can be controlled by that
 */
class Elevator{
public:
  /**
   * @enum DEVICE_ADDRESS
   * @brief Addresses of devices of elevator. Its values are correct for send to Elevator simulator.
   */
  enum DEVICE_ADDRESS : uint8_t {
    ADDR_EMERGENCY_BREAK  = 0x0F,
    ADDR_LED_OUT_P        = 0x10,
    ADDR_LED_OUT_1        = 0x11,
    ADDR_LED_OUT_2        = 0x12,
    ADDR_LED_OUT_3        = 0x13,
    ADDR_LED_OUT_4        = 0x14,
    ADDR_LED_IN_P         = 0x20,
    ADDR_LED_IN_1         = 0x21,
    ADDR_LED_IN_2         = 0x22,
    ADDR_LED_IN_3         = 0x23,
    ADDR_LED_IN_4         = 0x24,
    ADDR_DISPLAY          = 0x30,
    ADDR_BTN_IN_P         = 0xB0,
    ADDR_BTN_IN_1         = 0xB1,
    ADDR_BTN_IN_2         = 0xB2,
    ADDR_BTN_IN_3         = 0xB3,
    ADDR_BTN_IN_4         = 0xB4,
    ADDR_BTN_OUT_P        = 0xC0,
    ADDR_BTN_OUT_1        = 0xC1,
    ADDR_BTN_OUT_2        = 0xC2,
    ADDR_BTN_OUT_3        = 0xC3,
    ADDR_BTN_OUT_4        = 0xC4,
    ADDR_CONSOLE          = 0xD0,
    ADDR_SENSOR_P         = 0xE0,
    ADDR_SENSOR_1         = 0xE1,
    ADDR_SENSOR_2         = 0xE2,
    ADDR_SENSOR_3         = 0xE3,
    ADDR_SENSOR_4         = 0xE4,
    ADDR_CABIN            = 0xF0,
    ADDR_ENGINE           = 0xF1,
    ADDR_WATCH_DOG        = 0xFE
  };

private:
  static const uint8_t m_OWN_ADDRESS;       /// Address of controlling device (black freescale board).
  static const uint8_t m_REC_QUEUE_SIZE;    /// Size of queue where received packets will be sent.

  Uart          m_serial;                   /// Instance of UART object. It's used to communicate with elevator.
  QueueHandle_t m_recPackets;               /// Queue to which received packets will be sent.

  DIRECTION     m_lastDispDirection;        /// Remembering of last arrow direction displayed on display.
  char          m_lastDispText[STR_SIZE];   /// Remembering of last text displayed on display.

public:
  Elevator();
  virtual ~Elevator();

  // user control functions
public:
  void reset();

  void printText(const char* text);

  void setLedIN(LIFT_FLOOR floor, ON_OFF state);
  void setLedOUT(LIFT_FLOOR floor, ON_OFF state);
  void setAllLedsIN(ON_OFF state);
  void setAllLedsOUT(ON_OFF state);

  void setDisplay(DIRECTION direction, const char* text);
  void setDisplay(DIRECTION direction);
  void setDisplay(const char* text);
  void setDisplayFloor(LIFT_FLOOR floor);

  void setCabinLock(ON_OFF state);

  void setEmergencyBreak(ON_OFF state);

  void sendWatchDogSignal();
  void restoreWatchDog();

  void setEngineSpeed(DIRECTION direction, int8_t speed);
  void stopEngine();


  void requestEngineEncoder();
  void requestSensorValue(LIFT_FLOOR floor);
  void requestAllSensorsValue();
  void getNextInput(REC_PACKET_t* pPacket);
};

#endif /* INC_LIFTCOMUNICATOR_H_ */
