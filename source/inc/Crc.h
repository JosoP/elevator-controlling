/**
 * @file    Crc.h
 * @date    Jan 8, 2019
 * @author  JosoP
 *
 * @brief   Header file of Crc class.
 */

#ifndef INC_CRC_H_
#define INC_CRC_H_

#include <stdint.h>

/**
 *  @class  Crc
 *
 *  @brief  Class that represents CRC and is able to do CRC calculation with it
 */
class Crc {
private:
  static const uint8_t m_table[256];			  /// table of values used for CRC 8 calculating

  uint8_t m_currentValue;					          /// current CRC value

public:
  Crc();
  virtual ~Crc();

  void calculate(uint8_t inputData);
  void calculateMore(const uint8_t* inputData, uint8_t length);

  /********************************************************************************************************//**
   * @brief   Getter for current value of the CRC.
   * @return  Current value of the CRC.
   ***********************************************************************************************************/
  uint8_t getCurrentValue() const {
    return m_currentValue;
  }
};

#endif /* INC_CRC_H_ */
