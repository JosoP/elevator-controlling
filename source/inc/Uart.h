/**
 * @file    Uart.h
 * @date    Jan 17, 2019
 * @author  JosoP
 *
 * @brief   Header file of Uart class.
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include "FreeRTOS.h"
#include "event_groups.h"
#include <queue.h>

#define MAX_DATA_LENGTH     256                                 ///< maximal length of data in packet
#define MAX_PACKET_LENGTH   5 + MAX_DATA_LENGTH                 ///< maximal length of all packet

/**
 * @struct  REC_PACKET_t
 * @brief   Structure representing received DATA packet.
 */
typedef struct {
  uint8_t addrRx;                         ///< address of receiver
  uint8_t addrTx;                         ///< address of transmitter
  uint8_t dataLength;                     ///< length of data
  uint8_t data[20];                       ///< data
  uint8_t crc;                            ///< CRC of the packet (counted from addrRx, addrTx and data)
} REC_PACKET_t;

/**
 *  @class  Uart
 *
 *  @brief  Class for communicating with Elevator simulation APP
 */
class Uart {
public:
  Uart(uint32_t baudRate);
  virtual ~Uart();

public:
  static void registerRecQueue(QueueHandle_t queue);
  void sendPacket(uint8_t addrRX, uint8_t addrTX, const uint8_t* data, uint8_t dataLength);
};

#endif /* INC_UART_H_ */
