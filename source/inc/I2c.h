/**
 * @file    I2c.h
 * @date    Nov 5, 2018
 * @author  JosoP
 *
 * @brief   Header file of I2c class.
 */

#ifndef I2C_I2C_H_
#define I2C_I2C_H_

#include <fsl_i2c.h>

/**
 *  @class  I2c
 *
 *  @brief  Class that represents I2C interface of this device. It allows to do some basic operations.
 */
class I2c {
public:
	I2c();
	virtual ~I2c();

	bool write(uint8_t address, uint8_t* pData, size_t length);
	bool read(uint8_t address, uint8_t* pData, size_t length);
	bool readRegs(uint8_t addr, uint8_t reg, uint8_t * data, size_t length);
};

#endif /* I2C_I2C_H_ */
