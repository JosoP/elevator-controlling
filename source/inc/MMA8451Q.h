/**
 * @file    MMA8451Q.h
 * @date    Nov 5, 2018
 * @author  JosoP
 *
 * @brief   Header file of MMA8451Q class.
 */

#ifndef MMA8451Q_H
#define MMA8451Q_H


#include "I2c.h"

/**
 *  @class  MMA8451Q
 *
 *  @brief  Class that represents MMA8451Q accelerometer sensor.
 */
class MMA8451Q
{
public:
  /**
   * @enum INT_SOURCE
   * @brief Possible interrupt sources from INT_SOURCE accelerometer.
   */
	enum INT_SOURCE{
		INT_ASLP = 1<<7,
		INT_FIFO = 1<<6,
		INT_TRANS = 1<<5,
		INT_LNDPRT = 1<<4,
		INT_PULSE = 1<<3,
		INT_FF_MT = 1<<2,
		INT_DRDY = 1<<0
	};

  /**
   * @enum INT_POLARITY
   * @brief Possible polarities of interrupt signal.
   */
	enum INT_POLARITY{
		IPOL_HIGH,
		IPOL_LOW
	};

  /**
   * @enum INT_PIN
   * @brief Possible interrupt pins on the MMA8451Q accelerometer.
   */
	enum INT_PIN{
		INT_PIN1,
		INT_PIN2
	};

  /**
   * @enum PULSE_DIR
   * @brief Directions of the possible tap pulse.
   */
	enum PULSE_DIR{
		DIR_RIGHT,
		DIR_LEFT,
		DIR_FRONT,
		DIR_BACK,
		DIR_UP,
		DIR_DOWN,
		DIR_ERROR
	};


private:
	I2c m_i2c;
	int m_addr;

public:
  MMA8451Q(int addr);
  ~MMA8451Q();

  bool isConnected();

  float getAccX();
  float getAccY();
  float getAccZ();
  void getAccAllAxis(float * pResult);

  void enableTabDetection();
  void enableInterrupt(INT_SOURCE source, INT_POLARITY polarity, INT_PIN pin);
  void registerHandlerInt(void (*pCallback) (void));
  uint8_t getIntSource();
  PULSE_DIR getPulseDirection();

private:
  void readMoreRegs(uint8_t addr, uint8_t * data, int len);
  uint8_t readReg(uint8_t reg);
  void writeReg(uint8_t reg, uint8_t data);

  int16_t getAccAxis(uint8_t addr);
  void reset();
  void activate(bool activate);
};

#endif
