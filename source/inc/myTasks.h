/**
 * @file    myTasks.h
 * @date    Jan 20, 2019
 * @author  JosoP
 *
 * @brief   Header file where main tasks are declared.
 */


#ifndef MYTASKS_H_
#define MYTASKS_H_

#include "Elevator.h"

extern Elevator* pElevator;

extern QueueHandle_t emergency;

#ifdef __cplusplus
extern "C" {
#endif

void testTask(void *pvParameters);
void inputTask(void *pvParameters);
void movementTask(void *pvParameters);
void watchDogTask(void *pvParameters);
void blinkerTask(void *pvParameters);
void floorDiplayTask(void *pvParameters);
void emergencyTask(void *pvParameters);

#ifdef __cplusplus
}
#endif

#endif /* MYTASKS_H_ */
