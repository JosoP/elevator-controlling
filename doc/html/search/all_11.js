var searchData=
[
  ['waitstopfloor',['waitStopFloor',['../myTasks_8cpp.html#ac7a29d57f334c4e8b40df3a3614c90ed',1,'myTasks.cpp']]],
  ['watch_5fdog_5frefresh_5fms',['WATCH_DOG_REFRESH_MS',['../myTasks_8cpp.html#a3a778964c0c2db864a89663bedd18c11',1,'myTasks.cpp']]],
  ['watch_5fdog_5ftask_5fpriority',['WATCH_DOG_TASK_PRIORITY',['../definesRTOS_8h.html#a754f5ff668f7161e34d33e7ab8211ba4',1,'definesRTOS.h']]],
  ['watch_5fdog_5ftask_5fstack_5fsize',['WATCH_DOG_TASK_STACK_SIZE',['../definesRTOS_8h.html#a0d4a45d71af25b41a73da91818fa3f79',1,'definesRTOS.h']]],
  ['watchdogtask',['watchDogTask',['../myTasks_8h.html#a8cc6ecae5b915d449d26e566fe8261ba',1,'watchDogTask(void *pvParameters):&#160;myTasks.cpp'],['../myTasks_8cpp.html#a8cc6ecae5b915d449d26e566fe8261ba',1,'watchDogTask(void *pvParameters):&#160;myTasks.cpp']]],
  ['wide_5fproximity',['WIDE_PROXIMITY',['../Elevator_8h.html#af3b6a1a91b868b96b7a7f161706d17eca402c938783b707408d67ca5960017ae3',1,'Elevator.h']]],
  ['write',['write',['../classI2c.html#afb0e1ffd7b310e6e5fee55faf9b6fdce',1,'I2c']]]
];
