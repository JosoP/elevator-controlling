var searchData=
[
  ['read',['read',['../classI2c.html#a9eb6ea72abfa6423011d0c7f60c9e501',1,'I2c']]],
  ['readregs',['readRegs',['../classI2c.html#a6183e6e8ae5abcc833785d51d30a6e11',1,'I2c']]],
  ['receivetask',['receiveTask',['../Uart_8cpp.html#ab5a8dcaee9aeaf1154009baf09e9bed8',1,'Uart.cpp']]],
  ['receivetaskblocking',['receiveTaskBlocking',['../Uart_8cpp.html#ac951bf140e1c7bf9d815d2a864320c8e',1,'Uart.cpp']]],
  ['registerhandlerint',['registerHandlerInt',['../classMMA8451Q.html#af7b81374ff193e354839d27185f2b14a',1,'MMA8451Q']]],
  ['registerrecqueue',['registerRecQueue',['../classUart.html#ab289f190a22a3788a8418c3a6ab65e87',1,'Uart']]],
  ['repairbadstop',['repairBadStop',['../myTasks_8cpp.html#ae67d76af47d19ee04f7cf44c7ce33950',1,'myTasks.cpp']]],
  ['requestallsensorsvalue',['requestAllSensorsValue',['../classElevator.html#afe8eb91e3e3210d93dd90be13593b05c',1,'Elevator']]],
  ['requestengineencoder',['requestEngineEncoder',['../classElevator.html#a8b39bedec1b7f617ba9c292c178e61fb',1,'Elevator']]],
  ['requestsensorvalue',['requestSensorValue',['../classElevator.html#a707b6b138d90bc073cef54bef6d62c44',1,'Elevator']]],
  ['reset',['reset',['../classElevator.html#a2f788efba74811f9f47aec3834cb1272',1,'Elevator']]],
  ['restorewatchdog',['restoreWatchDog',['../classElevator.html#a82ae9fb6b71ec11d0bed16cad051ac20',1,'Elevator']]]
];
