var searchData=
[
  ['sendpacket',['sendPacket',['../classUart.html#aaedae2354a8abec1dd0d1ca811645bdd',1,'Uart']]],
  ['sendtaskblocking',['sendTaskBlocking',['../Uart_8cpp.html#af509c53d1b263fb06cefe4f347eb36a2',1,'Uart.cpp']]],
  ['sendwatchdogsignal',['sendWatchDogSignal',['../classElevator.html#ad6e59f52365c3e36abe76635f44fd865',1,'Elevator']]],
  ['sensorsignalreceived',['sensorSignalReceived',['../myTasks_8cpp.html#a45af47623b4e3570e753567fdec9a6ed',1,'myTasks.cpp']]],
  ['setallledsin',['setAllLedsIN',['../classElevator.html#a1064777c999fc9527180bbe58b02e0fe',1,'Elevator']]],
  ['setallledsout',['setAllLedsOUT',['../classElevator.html#ad1e727d7ff5e760faf37851a3cace0e2',1,'Elevator']]],
  ['setcabinlock',['setCabinLock',['../classElevator.html#a339935b39a540e53493610695693447b',1,'Elevator']]],
  ['setdisplay',['setDisplay',['../classElevator.html#a73d5e54fb90bd3ca525f53acad33fb64',1,'Elevator::setDisplay(DIRECTION direction, const char *text)'],['../classElevator.html#a0c025e8c0f1477594bd8bad5f529199c',1,'Elevator::setDisplay(DIRECTION direction)'],['../classElevator.html#ab190b2193dbcf9ccfd7bcc71a8aba4a3',1,'Elevator::setDisplay(const char *text)']]],
  ['setdisplayfloor',['setDisplayFloor',['../classElevator.html#acb691ba6646e95ed7893c634deb74d87',1,'Elevator']]],
  ['setemergencybreak',['setEmergencyBreak',['../classElevator.html#aa78380a0642ce594cded11b36abf9e9d',1,'Elevator']]],
  ['setenginespeed',['setEngineSpeed',['../classElevator.html#a53082a36ec11defdc800f3a000b17e16',1,'Elevator']]],
  ['setledin',['setLedIN',['../classElevator.html#ae20af19e4de905e738119576d0c6a39d',1,'Elevator']]],
  ['setledout',['setLedOUT',['../classElevator.html#a420b0aa76c26b5fff285b42b38fdb316',1,'Elevator']]],
  ['slowelevatordown',['slowElevatorDown',['../myTasks_8cpp.html#afa9de68d649f4dad7e6fc974defdea28',1,'myTasks.cpp']]],
  ['stopelevator',['stopElevator',['../myTasks_8cpp.html#a59880d0c1e46eabf3aba586205fb0ac8',1,'myTasks.cpp']]],
  ['stopengine',['stopEngine',['../classElevator.html#ab1711a46d3222ec0a3521797f2d82619',1,'Elevator']]]
];
