\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Crc Class Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Constructor \& Destructor Documentation}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}Crc()}{6}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}$\sim $Crc()}{6}{subsubsection.3.1.2.2}
\contentsline {subsection}{\numberline {3.1.3}Member Function Documentation}{6}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}calculate()}{6}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}calculateMore()}{6}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}getCurrentValue()}{7}{subsubsection.3.1.3.3}
\contentsline {section}{\numberline {3.2}Elevator Class Reference}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Constructor \& Destructor Documentation}{9}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Elevator()}{9}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}$\sim $Elevator()}{9}{subsubsection.3.2.2.2}
\contentsline {subsection}{\numberline {3.2.3}Member Function Documentation}{9}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}getNextInput()}{9}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}printText()}{9}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}requestAllSensorsValue()}{10}{subsubsection.3.2.3.3}
\contentsline {subsubsection}{\numberline {3.2.3.4}requestEngineEncoder()}{10}{subsubsection.3.2.3.4}
\contentsline {subsubsection}{\numberline {3.2.3.5}requestSensorValue()}{10}{subsubsection.3.2.3.5}
\contentsline {subsubsection}{\numberline {3.2.3.6}reset()}{10}{subsubsection.3.2.3.6}
\contentsline {subsubsection}{\numberline {3.2.3.7}restoreWatchDog()}{11}{subsubsection.3.2.3.7}
\contentsline {subsubsection}{\numberline {3.2.3.8}sendWatchDogSignal()}{11}{subsubsection.3.2.3.8}
\contentsline {subsubsection}{\numberline {3.2.3.9}setAllLedsIN()}{11}{subsubsection.3.2.3.9}
\contentsline {subsubsection}{\numberline {3.2.3.10}setAllLedsOUT()}{11}{subsubsection.3.2.3.10}
\contentsline {subsubsection}{\numberline {3.2.3.11}setCabinLock()}{12}{subsubsection.3.2.3.11}
\contentsline {subsubsection}{\numberline {3.2.3.12}setDisplay()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/3]}}{12}{subsubsection.3.2.3.12}
\contentsline {subsubsection}{\numberline {3.2.3.13}setDisplay()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/3]}}{12}{subsubsection.3.2.3.13}
\contentsline {subsubsection}{\numberline {3.2.3.14}setDisplay()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [3/3]}}{13}{subsubsection.3.2.3.14}
\contentsline {subsubsection}{\numberline {3.2.3.15}setDisplayFloor()}{13}{subsubsection.3.2.3.15}
\contentsline {subsubsection}{\numberline {3.2.3.16}setEmergencyBreak()}{13}{subsubsection.3.2.3.16}
\contentsline {subsubsection}{\numberline {3.2.3.17}setEngineSpeed()}{13}{subsubsection.3.2.3.17}
\contentsline {subsubsection}{\numberline {3.2.3.18}setLedIN()}{14}{subsubsection.3.2.3.18}
\contentsline {subsubsection}{\numberline {3.2.3.19}setLedOUT()}{14}{subsubsection.3.2.3.19}
\contentsline {subsubsection}{\numberline {3.2.3.20}stopEngine()}{14}{subsubsection.3.2.3.20}
\contentsline {section}{\numberline {3.3}I2c Class Reference}{15}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{15}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Constructor \& Destructor Documentation}{15}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}I2c()}{15}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}$\sim $I2c()}{16}{subsubsection.3.3.2.2}
\contentsline {subsection}{\numberline {3.3.3}Member Function Documentation}{16}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}read()}{16}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}readRegs()}{16}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3.3}write()}{17}{subsubsection.3.3.3.3}
\contentsline {section}{\numberline {3.4}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A8451Q Class Reference}{17}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{18}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Constructor \& Destructor Documentation}{18}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}MMA8451Q()}{18}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}$\sim $MMA8451Q()}{19}{subsubsection.3.4.2.2}
\contentsline {subsection}{\numberline {3.4.3}Member Function Documentation}{19}{subsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.3.1}enableInterrupt()}{19}{subsubsection.3.4.3.1}
\contentsline {subsubsection}{\numberline {3.4.3.2}enableTabDetection()}{19}{subsubsection.3.4.3.2}
\contentsline {subsubsection}{\numberline {3.4.3.3}getAccAllAxis()}{20}{subsubsection.3.4.3.3}
\contentsline {subsubsection}{\numberline {3.4.3.4}getAccX()}{20}{subsubsection.3.4.3.4}
\contentsline {subsubsection}{\numberline {3.4.3.5}getAccY()}{20}{subsubsection.3.4.3.5}
\contentsline {subsubsection}{\numberline {3.4.3.6}getAccZ()}{21}{subsubsection.3.4.3.6}
\contentsline {subsubsection}{\numberline {3.4.3.7}getIntSource()}{21}{subsubsection.3.4.3.7}
\contentsline {subsubsection}{\numberline {3.4.3.8}getPulseDirection()}{21}{subsubsection.3.4.3.8}
\contentsline {subsubsection}{\numberline {3.4.3.9}isConnected()}{21}{subsubsection.3.4.3.9}
\contentsline {subsubsection}{\numberline {3.4.3.10}registerHandlerInt()}{21}{subsubsection.3.4.3.10}
\contentsline {section}{\numberline {3.5}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}C\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}P\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}C\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}t Struct Reference}{22}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Detailed Description}{22}{subsection.3.5.1}
\contentsline {section}{\numberline {3.6}S\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}t Struct Reference}{23}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Detailed Description}{23}{subsection.3.6.1}
\contentsline {section}{\numberline {3.7}Uart Class Reference}{23}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Detailed Description}{23}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Constructor \& Destructor Documentation}{24}{subsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.2.1}Uart()}{24}{subsubsection.3.7.2.1}
\contentsline {subsubsection}{\numberline {3.7.2.2}$\sim $Uart()}{24}{subsubsection.3.7.2.2}
\contentsline {subsection}{\numberline {3.7.3}Member Function Documentation}{24}{subsection.3.7.3}
\contentsline {subsubsection}{\numberline {3.7.3.1}registerRecQueue()}{24}{subsubsection.3.7.3.1}
\contentsline {subsubsection}{\numberline {3.7.3.2}sendPacket()}{25}{subsubsection.3.7.3.2}
\contentsline {chapter}{\numberline {4}File Documentation}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Elevator Controlling.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{27}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{28}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Function Documentation}{28}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}accelIntHandler()}{28}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}main()}{28}{subsubsection.4.1.2.2}
\contentsline {section}{\numberline {4.2}inc/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Crc.h File Reference}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{29}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}inc/defines\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}OS.h File Reference}{29}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{30}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}inc/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Elevator.h File Reference}{30}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Detailed Description}{31}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Enumeration Type Documentation}{31}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}LIFT\_FLOOR}{31}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}SENSOR\_STATE}{32}{subsubsection.4.4.2.2}
\contentsline {section}{\numberline {4.5}inc/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I2c.h File Reference}{32}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Detailed Description}{32}{subsection.4.5.1}
\contentsline {section}{\numberline {4.6}inc/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A8451Q.h File Reference}{32}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Detailed Description}{33}{subsection.4.6.1}
\contentsline {section}{\numberline {4.7}inc/my\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Tasks.h File Reference}{33}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Detailed Description}{34}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Function Documentation}{34}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}blinkerTask()}{34}{subsubsection.4.7.2.1}
\contentsline {subsubsection}{\numberline {4.7.2.2}emergencyTask()}{34}{subsubsection.4.7.2.2}
\contentsline {subsubsection}{\numberline {4.7.2.3}floorDiplayTask()}{35}{subsubsection.4.7.2.3}
\contentsline {subsubsection}{\numberline {4.7.2.4}inputTask()}{35}{subsubsection.4.7.2.4}
\contentsline {subsubsection}{\numberline {4.7.2.5}movementTask()}{35}{subsubsection.4.7.2.5}
\contentsline {subsubsection}{\numberline {4.7.2.6}testTask()}{35}{subsubsection.4.7.2.6}
\contentsline {subsubsection}{\numberline {4.7.2.7}watchDogTask()}{37}{subsubsection.4.7.2.7}
\contentsline {section}{\numberline {4.8}inc/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Uart.h File Reference}{37}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Detailed Description}{38}{subsection.4.8.1}
\contentsline {section}{\numberline {4.9}mtb.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}c File Reference}{38}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}Detailed Description}{38}{subsection.4.9.1}
\contentsline {section}{\numberline {4.10}src/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Crc.cpp File Reference}{38}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}Detailed Description}{39}{subsection.4.10.1}
\contentsline {section}{\numberline {4.11}src/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Elevator.cpp File Reference}{39}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}Detailed Description}{39}{subsection.4.11.1}
\contentsline {section}{\numberline {4.12}src/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I2c.cpp File Reference}{39}{section.4.12}
\contentsline {subsection}{\numberline {4.12.1}Detailed Description}{39}{subsection.4.12.1}
\contentsline {section}{\numberline {4.13}src/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A8451Q.cpp File Reference}{40}{section.4.13}
\contentsline {subsection}{\numberline {4.13.1}Detailed Description}{41}{subsection.4.13.1}
\contentsline {subsection}{\numberline {4.13.2}Function Documentation}{41}{subsection.4.13.2}
\contentsline {subsubsection}{\numberline {4.13.2.1}PORTA\_IRQHandler()}{41}{subsubsection.4.13.2.1}
\contentsline {section}{\numberline {4.14}src/my\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Tasks.cpp File Reference}{41}{section.4.14}
\contentsline {subsection}{\numberline {4.14.1}Detailed Description}{43}{subsection.4.14.1}
\contentsline {subsection}{\numberline {4.14.2}Function Documentation}{43}{subsection.4.14.2}
\contentsline {subsubsection}{\numberline {4.14.2.1}arrivedToFloor()}{43}{subsubsection.4.14.2.1}
\contentsline {subsubsection}{\numberline {4.14.2.2}blinkerTask()}{44}{subsubsection.4.14.2.2}
\contentsline {subsubsection}{\numberline {4.14.2.3}choooseNextFloor()}{44}{subsubsection.4.14.2.3}
\contentsline {subsubsection}{\numberline {4.14.2.4}elevatorIddle()}{44}{subsubsection.4.14.2.4}
\contentsline {subsubsection}{\numberline {4.14.2.5}emergencyTask()}{45}{subsubsection.4.14.2.5}
\contentsline {subsubsection}{\numberline {4.14.2.6}findElevator()}{45}{subsubsection.4.14.2.6}
\contentsline {subsubsection}{\numberline {4.14.2.7}floorDiplayTask()}{45}{subsubsection.4.14.2.7}
\contentsline {subsubsection}{\numberline {4.14.2.8}getCurrentFloor()}{45}{subsubsection.4.14.2.8}
\contentsline {subsubsection}{\numberline {4.14.2.9}inputTask()}{46}{subsubsection.4.14.2.9}
\contentsline {subsubsection}{\numberline {4.14.2.10}moveElevator()}{46}{subsubsection.4.14.2.10}
\contentsline {subsubsection}{\numberline {4.14.2.11}movementTask()}{46}{subsubsection.4.14.2.11}
\contentsline {subsubsection}{\numberline {4.14.2.12}repairBadStop()}{47}{subsubsection.4.14.2.12}
\contentsline {subsubsection}{\numberline {4.14.2.13}sensorSignalReceived()}{47}{subsubsection.4.14.2.13}
\contentsline {subsubsection}{\numberline {4.14.2.14}slowElevatorDown()}{47}{subsubsection.4.14.2.14}
\contentsline {subsubsection}{\numberline {4.14.2.15}stopElevator()}{48}{subsubsection.4.14.2.15}
\contentsline {subsubsection}{\numberline {4.14.2.16}testTask()}{48}{subsubsection.4.14.2.16}
\contentsline {subsubsection}{\numberline {4.14.2.17}waitStopFloor()}{48}{subsubsection.4.14.2.17}
\contentsline {subsubsection}{\numberline {4.14.2.18}watchDogTask()}{48}{subsubsection.4.14.2.18}
\contentsline {subsection}{\numberline {4.14.3}Variable Documentation}{49}{subsection.4.14.3}
\contentsline {subsubsection}{\numberline {4.14.3.1}FLOOR\_BIT}{49}{subsubsection.4.14.3.1}
\contentsline {section}{\numberline {4.15}src/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Uart.cpp File Reference}{49}{section.4.15}
\contentsline {subsection}{\numberline {4.15.1}Detailed Description}{50}{subsection.4.15.1}
\contentsline {subsection}{\numberline {4.15.2}Function Documentation}{51}{subsection.4.15.2}
\contentsline {subsubsection}{\numberline {4.15.2.1}isAck()}{51}{subsubsection.4.15.2.1}
\contentsline {subsubsection}{\numberline {4.15.2.2}receiveTask()}{51}{subsubsection.4.15.2.2}
\contentsline {subsubsection}{\numberline {4.15.2.3}receiveTaskBlocking()}{51}{subsubsection.4.15.2.3}
\contentsline {subsubsection}{\numberline {4.15.2.4}sendTaskBlocking()}{53}{subsubsection.4.15.2.4}
\contentsline {chapter}{Index}{55}{section*.36}
